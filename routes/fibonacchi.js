const express = require('express');
const router = express.Router();
const fibonacchiFunctions = require('../functions/fibonacciFunctions');

// GET ALL THE POSTS
router.get('/:number', async (req, res) => {
  const index = parseInt(req.params.number) || null
  try {
    if (index === null) {
      throw new Error('Not a number was provided')
    }
    if (isNaN(index)) {
      throw new Error('Please provide a valid  number')
    }
    if (index <= 0) {
      throw new Error('Only number higher than 0 are accepted')
    }
    const result = await fibonacchiFunctions.getFibonacciSeries(index);
    res.json(result)
  } catch(err) {
    res.status(404).json({message: err.message})
  }

})

module.exports = router