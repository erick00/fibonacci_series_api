## Scenario
There is a need for a Rest (or Rest-like) API that can receive an index "n" and returns the Fibonacci value that corresponds to the given index, and you have been tasked with implementing the first version of this API.


## Solution
Based on the problem it's neccessary an end point that can return the Fibonacci series and with the index get the value of the position in the series.

The structure of the api is a basic one, separating the routes and function in diferents folders.
The function that return the fibonacci series is used in a recursive way, to potimize the code.
I used 2 diferent functions one to pass the index of the array and other to obtain the fibonacci series, with this i can create my object that return not only the value, it return the fibonacci series, the initial index and the value in the series.

Before to get the final result, validate if the index passed is a valid value, if it is continue to the function in other way return an error and a message indicating what was wrong.

## improves
It's a basic solution and a basic structure, in a future the api could include other components or routes and token validations for a secure  request.

## Note
I'm not a backend developer, but i try to do my best in the solution of the problem :)
