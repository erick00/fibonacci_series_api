const getResult = function (index) {
  if (index === 1) {
    return [0, 1];
  } else  {
    let series = getResult(index - 1);
    series.push(series[series.length - 1] + series[series.length - 2])
    return series
  }
}
const getFibonacciSeries = (index) => {
  const fibonacciSeries = getResult(index)
  return {
    fibonacci_series: fibonacciSeries,
    index: index,
    fibonacci_value: fibonacciSeries[index]
  }
}

module.exports = {
  getFibonacciSeries
}