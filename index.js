const express = require('express');
const app = express();
const cors = require('cors');

app.use(cors());

const fibonacciRoutes = require('./routes/fibonacchi');
app.use('/fibonacci', fibonacciRoutes)


app.get('/', (req, res)=> {
  res.send('I\'m alive');
});

app.listen(3000);